Ext.define('reportingApp.view.reports.ReportingViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.reportingviewcontroller',

    onEditCancelled: function (editor, value, startValue, eOpts) {
        var user = Ext._find(value.record.store.config.data.items, { name: value.record.data.name });
        Ext.Msg.confirm('Confirm', value.record.data.name + ': ' + reports.date + 'Confirm to continue', 'onConfirm', this);
    }
});
