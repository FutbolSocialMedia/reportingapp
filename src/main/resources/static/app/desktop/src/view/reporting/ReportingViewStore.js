Ext.define('reportingApp.view.reporting.ReportingViewStore', {
   extend: 'Ext.data.Store',
       alias: 'store.reportingviewstore',
       fields: [
           'id','name', 'type','email', 'created', 'date' , 'lastupdate','user'
       ],
       groupField: 'type',
       data: { items: [
           { id: 1, name: 'APAC Sales', type: '1', email: "jeanluc.picard@enterprise.com", created: 'Jeanluc Picard', date: '25 Jan 2020', lastupdate: '2 Feb 2021', user: 'Mr. Data' },
           { id: 2, name: 'AMER Sales', type: '1', email: "jordy@enterprise.com", created: 'Jordy', date: '10 Jun 2020', lastupdate: '8 Dec 2020', user: 'Jordy' },
           { id: 3, name: 'LATAM Sales', type: '1', email: "deanna.troi@enterprise.com", created: 'Deanna Troi', date: '7 Feb 2021', lastupdate: '12 Mar 2021', user: 'Mr. Data' },
           { id: 4, name: 'Datacenter Invoicing', type: '2', email: "mr.data@enterprise.com", created: 'Mr. Data', date: '1 Apr 2021', lastupdate: '17 Apr 2021', user: 'Mr. Data' }
       ]},
       proxy: {
           type: 'memory',
           reader: {
               type: 'json',
               rootProperty: 'items'
           }
       }
   });
