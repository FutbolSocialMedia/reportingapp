Ext.define('reportingApp.view.reporting.ReportingView',{
    extend: 'Ext.grid.Grid',
    xtype: 'reportingview',
    cls: 'reportingview',
    requires: ['Ext.grid.rowedit.Plugin'],
    controller: {type: 'reportingviewcontroller'},
    viewModel: {type: 'reportingviewmodel'},
    store: {type: 'reportingviewstore'},
    grouped: true,
    groupFooter: {
        xtype: 'gridsummaryrow'
    },
    plugins: {
        rowedit: {
            autoConfirm: false
        }
    },
    columns: [{
                 text: 'ID',
                 width: 50,
                 sortable: true,
                 dataIndex: 'id',
                 renderer: function(v, meta, rec) {
                     return rec.phantom ? '' : v;
                 }
             }, {
                 text: 'Name',
                 flex: 1,
                 sortable: true,
                 dataIndex: 'name',
                 field: {
                     xtype: 'textfield'
                 }
             }, {
                 header: 'Type',
                 width: 50,
                 sortable: true,
                 dataIndex: 'type',
                 field: {
                     xtype: 'textfield'
                 }
             }, {
                   text: 'Email',
                   flex: 1,
                   sortable: true,
                   dataIndex: 'email',
                   field: {
                       xtype: 'textfield'
                   }
               }, {
                   text: 'Creator',
                   flex: 1,
                   sortable: true,
                   dataIndex: 'created',
                   field: {
                       xtype: 'textfield'
                   }
               }, {
                  text: 'Date',
                  flex: 1,
                  sortable: true,
                  dataIndex: 'date',
                  field: {
                      xtype: 'textfield'
                  }
              }, {
                  text: 'Last Update',
                  flex: 1,
                  sortable: true,
                  dataIndex: 'lastupdate',
                  field: {
                      xtype: 'textfield'
                  }
              }, {
                  text: 'User',
                  flex: 1,
                  sortable: true,
                  dataIndex: 'user',
                  field: {
                      xtype: 'textfield'
                  }
              }],
    listeners: {
        canceledit: 'onEditCancelled'
    }
});
