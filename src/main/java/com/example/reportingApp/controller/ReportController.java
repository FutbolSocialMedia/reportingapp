/*
 * Copyright (c) 2021. Tony De Biase, This source is a part of
 * DataArt code sample Requested to Aldo Antonio De Biase, as an interview Exercise.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.example.reportingApp.controller;


import com.example.reportingApp.exception.ResourceNotFoundException;
import com.example.reportingApp.model.Report;
import com.example.reportingApp.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* The type Report controller.
*
* @author Tony De Biase
*/
@RestController
@RequestMapping("/api/v1")
public class ReportController {

    @Autowired
    private ReportRepository reportRepository;

    /**
     * Get all reports list.
     *
     * @return the list
     */
    @GetMapping("/reports")
    public List<Report> getAllReports() {
        return reportRepository.findAll();
    }

    /**
     * Gets Reports by id.
     *
     * @param reportId the Report id
     * @return the Report by id
     * @throws ResourceNotFoundException the resource not found exception
     */
    @GetMapping("/reports/{id}")
    public ResponseEntity<Report> getReportById(@PathVariable(value = "id") Long reportId)
            throws ResourceNotFoundException {
        Report report =
                reportRepository
                        .findById(reportId)
                        .orElseThrow(() -> new ResourceNotFoundException("Report not found on :: " + reportId));
        return ResponseEntity.ok().body(report);
    }

    /**
     * Create report report.
     *
     * @param report the report
     * @return the report
     */
    @PostMapping("/reports")
    public Report createReport(@Valid @RequestBody Report report) {
        return reportRepository.save(report);
    }

    /**
     * Update report response entity.
     *
     * @param reportId the report id
     * @param reportDetails the report details
     * @return the response entity
     * @throws ResourceNotFoundException the resource not found exception
     */
    @PutMapping("/reports/{id}")
    public ResponseEntity<Report> updateReport(
            @PathVariable(value = "id") Long reportId, @Valid @RequestBody Report reportDetails)
            throws ResourceNotFoundException {

        Report report =
                reportRepository
                        .findById(reportId)
                        .orElseThrow(() -> new ResourceNotFoundException("Report not found on :: " + reportId));

        report.setEmail(reportDetails.getEmail());
        report.setReportType(reportDetails.getReportType());
        report.setReportName(reportDetails.getReportName());
        report.setUpdatedAt(new Date());
        final Report updatedReport = reportRepository.save(report);
        return ResponseEntity.ok(updatedReport);
    }

    /**
     * Delete report map.
     *
     * @param reportId the report id
     * @return the map
     * @throws Exception the exception
     */
    @DeleteMapping("/report/{id}")
    public Map<String, Boolean> deleteReport(@PathVariable(value = "id") Long reportId) throws Exception {
        Report report =
                reportRepository
                        .findById(reportId)
                        .orElseThrow(() -> new ResourceNotFoundException("Report not found on :: " + reportId));

        reportRepository.delete(report);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}


