/*
 * Copyright (c) 2021. Tony De Biase, This source is a part of
 * DataArt code sample Requested to Aldo Antonio De Biase, as an interview Exercise.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.example.reportingApp.repository;

import com.example.reportingApp.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Report repository.
 *
 * @author Tony De Biase
 */
@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {}
